<?php 
require 'animal.php';
require 'frog.php';
require 'ape.php';

$sheep = new Animal("shaun");
// var_dump($sheep);

echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded; // false
echo "<br><br>";

$kodok = new Frog("buduk");
echo "<br>";
echo $kodok->name; // "shaun"
echo "<br>";
echo $kodok->legs; // 2
echo "<br>";

$kodok->jump() ; // "hop hop"
echo "<br><br>";

$sunGoKong = new Ape("kera sakti");
echo "<br>";
echo $sunGoKong->name; // "shaun"
echo "<br>";
echo $sunGoKong->legs; // 2
echo "<br>";

$sunGoKong->yell() ; 
echo "<br><br>";

 ?>